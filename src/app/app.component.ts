import { AfterViewInit, Component, NgZone, OnInit, ViewEncapsulation } from '@angular/core';
import { MapsApiLoader } from './maps/maps-api-loader';


declare const window: any;
declare const LMInit: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'locus-map';

  constructor(private mapsApiLoader: MapsApiLoader, private ngZone: NgZone) {
  }

  ngOnInit() {
    
  }

  ngAfterViewInit() {
    LMInit.loadMap('#map')
      .then(m => { window.map = m })
      .catch(e => console.error('Error initializing map: ', e))


    // await this.mapsApiLoader.loadMap();
    
  }
}
