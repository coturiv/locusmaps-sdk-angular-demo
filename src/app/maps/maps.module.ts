import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapsOption, MapsOptionToken } from './maps-api-loader';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class MapsModule {
  static withConfig(option: MapsOption): ModuleWithProviders {
    return {
      ngModule: MapsModule,
      providers: [{
        provide: MapsOptionToken,
        useValue: option
      }]
    }; 
  }
}
