import { Injectable, Inject, InjectionToken } from '@angular/core';

export interface MapsOption {
    venueId: string,
    accountId: string,
    headless?: boolean,
}

export const MapsOptionToken = new InjectionToken<MapsOption>('maps-option');


const API_PATH = 'https://maps.locuslabs.com/sdk/dist/current/' + 'main.js';

@Injectable({
    providedIn: 'root'
})
export class MapsApiLoader {
  
    mapState: 'loading' | 'loaded' | undefined;
  
    constructor(@Inject(MapsOptionToken) private mapOption: MapsOption) {
      	// this.loadMap();
    }
  
    loadMap() {
		return new Promise((resolve) => {
			if (this.mapState) {
				return;
			}
	  
			this.mapState = 'loading';
			const mapsScript = document.createElement('script');
			mapsScript.async = false;
			mapsScript.src = new URL(API_PATH).href;
		
			mapsScript.onload = () => {
				this.mapState = 'loaded';

				resolve(this.mapState);
			};
		
			document.head.appendChild(mapsScript);
		})
	}
}

  