import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MapsModule } from './maps/maps.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    MapsModule.withConfig({
      venueId: 'lax',
      accountId: 'A11F4Y6SZRXH4X',
      headless: false,
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
